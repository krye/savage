<img src="https://gitlab.com/lambda-dynamics/savage/-/raw/master/banner.png">

# SaVaGe

Dynamic SVG-based graphics for event signage and video overlays

## Download

Releases for Windows & Linux can be downloaded from ... our non-existant website.
We could use a Mac maintainer to help built and test releases!

## Run from Source

You need nodejs and yarn installed for development.

~~~
git clone https://gitlab.com/lambda-dynamics/savage.git
cd savage
yarn install
yarn run run
~~~

### Building

Yarn will install `electron-build` (and `electron-build-squirrel-windows` on Windows) to bundle things up.

~~~
yarn run build
~~~

## Preparing Screens

SaVaGe expects screens in [Inkscape](https://inkscape.org/)-formatted SVG. 
Adding `data-` tags to elements of the SVG file makes them react to changes you make on the dashboard or through the API.
See the wiki page for more information on preparing screens.

SaVaGe also can pull in static graphics (JPG, PNG), audio (MP3, OGG, WAV), and video (MP4, OGV) files that can be referenced from your screens.