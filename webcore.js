const express = require('express')
const app = express()
var http = require('http').createServer(app)
var io = require('socket.io')(http)
const fs = require('fs')
const path = require('path')
const gm = require('gray-matter')
const cors = require('cors')
const fileUpload = require('express-fileupload');


function sortObject(o) {
    return Object.keys(o).sort().reduce((r, k) => (r[k] = o[k], r), {});
}

var displayState = {
  '_id': 'displayState',
  screens: [],
  packs: [],
  images: [],
  videos: [],
  sounds: [],
  layers: {},
  variables: {},
  rotators: {},
  current: null,
  currentVideo: '',
  currentPoster: '',
}

const port = 9400

app.use(express.json())
app.use(cors())
app.use(fileUpload());

if(fs.existsSync(path.join(global.filePath, 'states', `default.json`))) {
  displayState = JSON.parse(fs.readFileSync(path.join(global.filePath, 'states', `default.json`)))
  io.emit('command', {cmd: 'reload'});
}

app.get('/snapshots', (req, res) => {
  saveStates = fs.readdirSync(path.join(global.filePath, 'states'))
  arr = []
  saveStates.forEach( file => {
    arr.push(path.basename(file, '.json'))
  })
  res.send(JSON.stringify(arr))
})

app.get('/resources', (req, res) => {
  files = fs.readdirSync(path.join(global.filePath, 'resources'))
  files.forEach( file => {
    const ext = path.extname(file).toLowerCase()
    const base = path.basename(file)
    if(ext === '.svg') {
      if(!displayState.screens.includes(file)) displayState.screens.push(file)
      var svg = fs.readFileSync(path.join(global.filePath, 'resources', file), 'utf8')

      var matches = Array.from(svg.matchAll(/inkscape:label="([.a-zA-Z0-9- ]+)"?/gi))
      matches.forEach( (m) => {
        if(!Object.keys(displayState.layers).includes(m[1])) {
          displayState.layers[m[1]] = false
        }
      })

      matches = Array.from(svg.matchAll(/data-variable="([.a-zA-Z0-9- ]+)"?/gi))
      matches.forEach( (m) => {
        if(!Object.keys(displayState.variables).includes(m[1])) {
          displayState.variables[m[1]] = ''
        }
      })

      matches = Array.from(svg.matchAll(/data-background="([.a-zA-Z0-9- ]+)"?/gi))
      matches.forEach( (m) => {
        if(!Object.keys(displayState.variables).includes(m[1])) {
          displayState.variables[m[1]] = ''
        }
      })

      matches = Array.from(svg.matchAll(/data-image="([.a-zA-Z0-9- ]+)"?/gi))
      matches.forEach( (m) => {
        if(!Object.keys(displayState.variables).includes(m[1])) {
          displayState.variables[m[1]] = ''
        }
      })

      matches = Array.from(svg.matchAll(/data-rotator="([a-zA-Z0-9- ]+)"?/gi))
      matches.forEach( (m) => {
        if(!Object.keys(displayState.rotators).includes(m[1])) {
          displayState.rotators[m[1]] = {
            files: [],
            current: 0,
            delay: 10000
          }
        }
      })
    }
    if(ext === '.mp3') {
      if(!displayState.sounds.includes(file)) {
        displayState.sounds.push(file)
      }
    }
    if(ext === '.png' || ext === '.jpg') {
      if(!displayState.images.includes(file)) {
        displayState.images.push(file)
      }
    }
    if(ext === '.mp4' || ext === '.ogv' || ext === '.mov' || ext === '.mkv') {
      if(!displayState.videos.includes(file)) {
        displayState.videos.push(file)
      }
    }
    if(ext === '.pack') {
      if(!displayState.packs.includes(file)) {
        packData = gm(fs.readFileSync(path.join(global.filePath, 'resources', file), 'utf8'))
        if(packData && !packData.isEmpty) {
          displayState.packs.push(base)

          if(packData.data['variables']) {
            packData.data.variables.forEach( (v) => {
              if(!Object.keys(displayState.variables).includes(v)) {
                displayState.variables[v] = ''
              }
            })
          }
        }
      }
    }
  })
  displayState.images.sort();
  displayState.videos.sort();
  displayState.sounds.sort();
  displayState.screens.sort();
  displayState.layers = sortObject(displayState.layers)
  displayState.variables = sortObject(displayState.variables)
  res.send(JSON.stringify(displayState))
})

app.get('/packs/:key', (req, res) => {
  packData = gm(fs.readFileSync(path.join(global.filePath, 'resources', req.params['key'] + '.pack'), 'utf8'))

  if(packData && !packData.isEmpty) {
    res.send(packData.content)
  }

})

app.post('/cmd/setText', (req, res) => {
  io.emit('command', {cmd: 'setText', 'key': req.query['key'], value: req.query['value']})
  displayState.variables[req.query['key']] = req.query['value']
  res.send('OK')
})

app.post('/cmd/switch', (req, res) => {
  io.emit('command', {cmd: 'switch', 'screen': req.query['screen'], 'cut': req.query['cut']})
  displayState.current = req.query['screen']
  res.send('OK')
})

app.post('/upload', (req, res) => {
  if(!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).send('No files were uploaded')
  }

  Object.values(req.files).forEach( (file) => {
    let uploadPath = path.join(global.filePath, 'resources', file.name)
    file.mv(uploadPath, (err) => {
      if(err) {
        return res.status(500).send(err)
      }

      res.redirect('back')
    })
  })

})

app.use('/resource-files', express.static(path.join(global.filePath, 'resources')));
app.use('/', express.static(path.join(__dirname, 'src')));

io.on('connection', function(socket){

  socket.on('command', function(msg){
    io.emit('command', msg);
    switch(msg.cmd) {
      case 'switch':
        displayState.current = msg.screen
        break
      case 'showLayer':
        displayState.layers[msg.layer] = true
        break
      case 'hideLayer':
        displayState.layers[msg.layer] = false
        break
      case 'setText':
        displayState.variables[msg.key] = msg.value
        break
      case 'updateRotator':
        if(!displayState.rotators[msg.key]) displayState.rotators[msg.key] = {}
        displayState.rotators[msg.key].files = msg.files
        displayState.rotators[msg.key].delay = msg.delay
        break
      case 'loadVideo':
        displayState.currentVideo = msg.video
        break
      case 'playVideo':
        break
      case 'stopVideo':
        break
      case 'setPoster':
        displayState.currentPoster = msg.poster
        break
      case 'saveConfig':
        console.log(msg.value)
        fs.writeFile(path.join(global.filePath, 'states', `${msg.value}.json`), JSON.stringify(displayState), (e) => {
          if(e) {
            console.log(e)
          }
        })
        break
      case 'loadConfig':
        if(fs.existsSync(path.join(global.filePath, 'states', `${msg.value}`))) {
          console.log('loading ', )
          displayState = JSON.parse(fs.readFileSync(path.join(global.filePath, 'states', `${msg.value}`)))
        }
        break
    }
  });
});

http.listen(port, function(){
  console.log(`webcore listening on *:${port}`);
  console.log(`serving resources from ${global.filePath}`)
});
